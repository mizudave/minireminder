import React, { Component } from 'react'
import { Actions } from 'react-native-router-flux'
import axios from 'axios'
import { Text, 
    View, 
    TextInput, 
    StyleSheet,
    Button, 
    TouchableOpacity,
    ImageBackground,
    ScrollView,
    Alert,
    AsyncStorage} from 'react-native'

const styles = StyleSheet.create ({

    container : {
        flex : 1,
        justifyContent : 'center',
        alignItems : 'center',
        marginTop: 200
    },

    username : {
        height : 40,
        width : 200,
        borderWidth : 2,
        borderColor : 'skyblue',
        textAlign : 'center',
        borderRadius : 10,
    },

    password : {
        height : 40,
        width : 200,
        borderWidth : 2,
        borderColor : 'skyblue',
        textAlign : 'center',
        borderRadius : 10,
    },

    login : {
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : 'skyblue',
        padding : 10, 
        bottom : 200,
        left : 2,
        borderRadius : 10,
        width : 180,
    },

    textSignup : {
        color : 'white', 
        marginTop: 200
    }

});



export default class LoginReg extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email : '',
            password: '',
            token : '',
        }
        console.log(this.state.token)
    }

    loginUser = () => {
        
        axios.post("https://appdoto.herokuapp.com/api/users/login",
        {
                email : this.state.email,
                password : this.state.password,
        })

        .then (resp=> 
            console.log(resp.data)
        )

        .then (resp => {
            this.setState({
                token : resp.data.data.token
            })

        })

        .then (resp => {
            async saveItem (token, value)
                try {
                    await AsyncStorage.getItem(token, value);
                }
                catch (error) {
                    
                }
        })
    }


  render() {
    return (

        <ImageBackground source = {require('../img/bg.jpg')} 
                style = {{width: '100%', height: '100%'}}>

        <ScrollView>
        
            <View style = {styles.container}> 

                <Text>
                    Email :
                </Text>
                <TextInput
                    style = {styles.username} 
                    onChangeText = {(email) => this.setState({email})} 
                    value = {this.state.email} placeholder = 'Email' />

                <Text>
                    Password :
                </Text>
                <TextInput 
                    secureTextEntry
                    style = {styles.password} 
                    onChangeText = {(password) => this.setState({password})}
                    value = {this.state.password} placeholder = 'Password' />
                
                <Text style = {styles.textSignup} >
                    Doesn't have account : <Text onPress = {() => Actions.daftar()}
                        style = {{fontWeight : 'bold'}}> Sign Up </Text>
                </Text>

                <TouchableOpacity onPress = {this.loginUser} style = {styles.login} >
                    <Text> Log In </Text>
                </TouchableOpacity>

                {/* <TouchableOpacity onPress = {() => Actions.todo()} style = {styles.back}>
                    <Text> Login Page </Text>
                </TouchableOpacity> */}

            </View>

        </ScrollView>

        </ImageBackground>

    )
  }
}



// axios.post("https://remindie.herokuapp.com/user/login",
// {
// username: this.state.username,
// password: this.state.password,
// })

// .then(()=> {this.setState({username:'',password:''})})

// .then(function(){
// Actions.todo();
// })


// };

// loginUser = () => {
//     console.log('aku diklik')
//     const { password, email } = this.state;
//     axios({
//       method: 'post',
//       url: 'https://to-do-app-fzd.herokuapp.com/user/login',
//       data: {
//         'email': this.state.email,
//         'password': this.state.password,
//       },
//       headers: {
//         'Content-type' : 'application/json',
//         // 'X-Requested-With': 'XMLHttpRequest'
//       }
//     }) 
//     .then((response) => {
//         this.setState({token : response.data.token})
//     })
//     .then(() => {
//         Actions.todo({token : this.state.token})
//     })
//     .catch((error) => {
//       Alert.alert("error",error)
//     });
// }