import React, { Component } from 'react'
import { Text, 
  View,
  StyleSheet,
  TouchableOpacity } from 'react-native'

const styles = StyleSheet.create ({

  list : {
    position : 'relative',
    padding : 20,
    paddingRight : 100,
    borderBottomWidth : 2,
    borderBottomColor : 'pink', 
  },

  listText : {
    paddingLeft : 20,
    borderLeftWidth : 10,
    borderLeftColor : 'white'
  },

  listDelete : {
    position : 'absolute',
    justifyContent : 'center',
    alignItems : 'center',
    backgroundColor : 'red',
    padding : 10,
    top : 10,
    bottom : 10,
    right : 10,  
  },

  deleteText : {
    color : 'white',
  },

  editText : {
    color : 'white',
  },

  listEdit : {
    position : 'absolute',
    justifyContent : 'center',
    alignItems : 'center',
    backgroundColor : 'orange',
    padding : 10,
    top : 10,
    bottom : 10,
    right : 50,  
  },

});

export default class InputTo extends Component {

  render() {
    return (

      <View key = {this.props.keyval} style = {styles.list}>

        <Text style = {styles.listText}> {this.props.val.date} </Text>
        <Text style = {styles.listText}> {this.props.val.list} </Text>

        <TouchableOpacity onPress = {this.props.delete} style = {styles.listDelete}>
          <Text style = {styles.deleteText}> D </Text>
        </TouchableOpacity>

        <TouchableOpacity onPress = {this.props.edit} style = {styles.listEdit}>
          <Text style = {styles.editText}> E </Text>
        </TouchableOpacity>
        
      </View>

    )
  }
}
