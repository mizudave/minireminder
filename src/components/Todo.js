import React, { Component } from 'react'
import InputTo from './InputTo.js'
import axios from 'axios'
import { Actions } from 'react-native-router-flux'
import { Text, 
    View,
    TextInput,
    ScrollView,
    TouchableOpacity,
    StyleSheet,
    ImageBackground,
    } from 'react-native'

const styles = StyleSheet.create ({

    container : {
        flex : 1,
    },

    header : {
        backgroundColor : 'skyblue',
        alignItems : 'center',
        justifyContent : 'center',
        borderBottomWidth : 5,
        borderBottomColor : 'pink',
    },

    headerText : {
        color : 'red',
        fontSize : 20,
        padding : 26,
    },

    scrollContainer : {
        flex : 1,
        marginBottom : 100,
    },

    footer : {
        position : 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 10,
    },

    textInput : {
        width: 250,
        alignSelf : 'stretch',
        color : 'white',
        padding : 5,
        backgroundColor : 'transparent',
        borderWidth: 2,
        borderColor: 'skyblue',
        borderRadius : 20,
        left : 20,
    },

    addButton : {
        position : 'absolute',
        zIndex : 11,
        right : 20,
        bottom : 15,
        backgroundColor : 'skyblue',
        width : 60,
        height : 60,
        borderRadius : 50,
        alignItems : 'center',
        justifyContent : 'center',
        elevation : 8,
    },

    addButtonText : {
        color : 'red',
        fontSize : 24,
    },

});

export default class Todo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            listArray : [],
            listText : '',
            key :''
        }
    }

    addDo = () => {
        
        axios.post("https://to-do-app-fzd.herokuapp.com/api/v1/do/create",
        {
            body : {
             title : this.state.listText,
            },
            headers : {
                'Content-type' : 'application/json', 
                'Authorization' : this.props.token 
            }
        }   
        )


        .then(()=> {this.setState({title:''})})

        .then(function(){
        this.addList();
    })}

  render() {

    let lists = this.state.listArray.map((val, key) => {
        return <InputTo key = {key} keyval = {key} val = {val}
            delete = { ()=> this.deleteList(key) } 
            edit = { ()=> this.editList(key) } />
    });

    return (

    <ImageBackground source = {require('../img/bg.jpg')} 
        style = {{width: '100%', height: '100%'}}>

      <View style = {styles.container}>

        <View style = {styles.header}>

            <Text style = {styles.headerText}> Mini Reminder </Text>

        </View>

        <ScrollView style = {styles.scrollContainer}>
            {lists}
        </ScrollView>

        <View style = {styles.footer}>

            <TextInput style = {styles.textInput} 
                onChangeText = {(listText) => this.setState({listText})}
                value = {this.state.listText}
                placeholder = 'Add Item' placeholderTextColor = '#ffa803'
                underlineColorAndroid = 'transparent' textAlign = 'center' >
            </TextInput>

        </View>

        <TouchableOpacity 
            style = {styles.addButton}
            onPress = {this.addDo}>
                <Text style = {styles.addButtonText}> + </Text>
        </TouchableOpacity>
        
      </View>

      </ImageBackground>
    )
  }

  addList(add) {
    let list = this.state.listArray;
    let index = this.state.key;
    if (index !== '') {
        list[index].list = this.state.listText;
        this.setState({ listArray : list, listText : ''})
    }
    
    else {
        var a = new Date();
        this.state.listArray.push({
            'date' : a.getFullYear() + 
            "/" + (a.getMonth() + 1) +
            "/" + a.getDate(),
            'list' : this.state.listText
        });
        this.setState({ listArray : this.state.listArray })
        this.setState({ listText : '' });
    }
  }

  deleteList(key) {
      this.state.listArray.splice(key, 1);
      this.setState({ listArray : this.state.listArray })
  }

  editList(key) {
      this.setState({ listText : this.state.listArray[key].list,
        key : key})
  }

}
