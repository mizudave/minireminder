import React, { Component } from 'react'
import { Actions } from 'react-native-router-flux';
import axios from 'axios'
import { Text, 
    View, 
    TextInput, 
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    AsyncStorage,
    ScrollView
     } from 'react-native'


const styles = StyleSheet.create ({

    container : {
      flex : 1,
      justifyContent : 'center',
      alignItems : 'center', 
      marginTop: 100
    },

    username : {
        height : 40,
        width : 200,
        borderWidth : 2,
        borderColor : 'skyblue',
        textAlign  : 'center',
        borderRadius : 10,
    },

    password : {
        height : 40,
        width : 200,
        borderWidth : 2,
        borderColor : 'skyblue',
        borderRadius : 10,
        textAlign : 'center'
    },

    retype : {
        height : 40,
        width : 200,
        borderWidth : 2,
        borderColor : 'skyblue',
        borderRadius : 10,
        textAlign : 'center'
    },

    email : {
        height : 40,
        width : 200,
        borderWidth : 2,
        borderColor : 'skyblue',
        borderRadius : 10,
        textAlign :  'center'
    },

    nama : {
        height : 40,
        width : 200,
        borderWidth : 2,
        borderColor : 'skyblue',
        borderRadius : 10,
        textAlign : 'center'
    },

    daftar : {
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : 'skyblue',
        padding : 10, 
        borderRadius : 10,
        width : 180,
        marginTop: 20
    },

    back : {
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : 'skyblue',
        padding : 10, 
        borderRadius : 10,
        width : 180,
        marginTop: 20
    },

});

export default class Daftar extends Component {

    constructor(props){
        super(props);
        this.state = {
            username : '',
            password : '',
            retype : '',
            firstName : '',
            lastName : '',
            email : '',
        }
      }
      
      daftarUser = () => {
        console.log('aku diklik')
        const { username, password, retype, firstName, lastName, email } = this.state;
        axios({
          method: 'post',
          url: 'https://appdoto.herokuapp.com/api/users/',
          data: {
            'username': this.state.username,
            'fullname': this.state.firstName,
            // 'lastname': this.state.lastName,
            'email': this.state.email,
            'password': this.state.password,
            // 'confirmpassword': this.state.retype,
          },
          headers: {
            'Content-type' : 'application/json',
            // 'X-Requested-With': 'XMLHttpRequest'
          }
        })
        .then((response) => {
          Actions.pop()
        })
        .catch((error) => {
          console.log("error",error)
        });
    }

  render() {
    console.log(this.state)
    return (

      <ImageBackground source = {require('../img/bg.jpg')} 
                style = {{width: '100%', height: '100%'}}>
      <ScrollView>
        <View style = {styles.container}>
          
          <Text> First Name : </Text>
          <TextInput
            style = {styles.nama}
            onChangeText = {(firstName) => this.setState({firstName})}
            placeholder = 'First Name' />

          {/* <Text> Last Name : </Text>
          <TextInput
            style = {styles.nama}
            onChangeText = {(lastName) => this.setState({lastName})}
            placeholder = 'Last Name' /> */}

          <Text> Email : </Text>
          <TextInput
            style = {styles.email}
            onChangeText = {(email) => this.setState({email})}
            value = {this.state.email} placeholder = 'Email' />

          <Text> Username : </Text>
          <TextInput 
            style = {styles.username} 
            onChangeText = {(username) => this.setState({username})} 
            value = {this.state.username} placeholder = 'Username' />
          
          <Text> Password : </Text>        
          <TextInput 
            secureTextEntry
            style = {styles.password}
            onChangeText = {(password) => this.setState({password})}
            value = {this.state.password} placeholder = 'Password' />

          {/* <Text> Retype Password : </Text>
          <TextInput 
            secureTextEntry
            style = {styles.retype}
            onChangeText = {(retype) => this.setState({retype})}
            value = {this.state.retype} placeholder = 'Retype Password' /> */}
          
          <TouchableOpacity onPress = {this.daftarUser} style = {styles.daftar}>
            <Text> Register </Text>
          </TouchableOpacity>

          <TouchableOpacity onPress = {() => Actions.loginreg()} style = {styles.back}>
            <Text> Login Page </Text>
          </TouchableOpacity>

        </View>
      </ScrollView>
      </ImageBackground>
    )
  }

}
