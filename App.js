import React, {Component} from 'react';
import { Router, Scene } from 'react-native-router-flux'
import LoginReg from './src/components/LoginReg.js'
import Tombol from './src/components/Tombol.js'
import Daftar from './src/components/Daftar'
import InputTo from './src/components/InputTo.js'
import Todo from './src/components/Todo.js'
import { Actions } from 'react-native-router-flux'
import {Platform, 
    StyleSheet, 
    Text, 
    View, 
    ImageBackground} from 'react-native';

const styles = StyleSheet.create({

});

export default class App extends Component {
  render() {
    return (

          <Router>
            <Scene  hideNavBar = 'true' key ="root">
              <Scene key = "loginreg" component = {LoginReg} title = "Login" />
              <Scene key = "daftar" component = {Daftar} title = "Tombol" />
              <Scene key = "todo" component = {Todo} title = "Todo" />
            </Scene>
          </Router>

    );
  }
}

